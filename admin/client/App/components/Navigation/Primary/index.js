/**
 * The primary (i.e. uppermost) navigation on desktop. Renders all sections and
 * the home-, website- and signout buttons.
 */

import React from 'react'
import { Container } from '../../../elemental'
import PrimaryNavItem from './NavItem'

var PrimaryNavigation = React.createClass({
	displayName: 'PrimaryNavigation',
	propTypes: {
		brand: React.PropTypes.string,
		currentSectionKey: React.PropTypes.string,
		sections: React.PropTypes.array.isRequired,
		signoutUrl: React.PropTypes.string
	},
	state: {
		user: {
			collegeDetails: {
				name: ''
			}
		}
	},
	getInitialState () {
		return {}
	},
	// Handle resizing, hide this navigation on mobile (i.e. < 768px) screens
	componentDidMount () {
		fetch('/api/user')
			.then(e => e.json())
			.then(e => {
				this.setState({
					user: e.data
				})

				if(Keystone)
					Keystone.permissionUser = e.data;
			})
		this.handleResize()
		window.addEventListener('resize', this.handleResize)
	},
	componentWillUnmount () {
		window.removeEventListener('resize', this.handleResize)
	},
	handleResize () {
		this.setState({
			navIsVisible: window.innerWidth >= 768
		})
	},
	// Render the sign out button
	renderSignout () {
		if (!this.props.signoutUrl) return null

		return (
			<PrimaryNavItem
				label='octicon-sign-out'
				href={this.props.signoutUrl}
				title='Sign Out'
			>
				<span className='octicon octicon-sign-out' />
			</PrimaryNavItem>
		)
	},
	// Render the back button
	renderBackButton () {
		// if (!Keystone.backUrl) return null
		return (
			<PrimaryNavItem
				label='octicon-globe'
				href={Keystone.backUrl}
				title={'Front page - ' + this.props.brand}
			>
				<div style={{ textAlign: 'center' }}>
					{this.state.user &&
						this.state.user.collegeDetails &&
						this.state.user.collegeDetails.name}{' '}
					<br /> ({this.props.user.name})
				</div>
			</PrimaryNavItem>
		)
	},
	// Render the link to the webpage
	renderFrontLink () {
		return (
			<ul className='app-nav app-nav--primary app-nav--right'>
				{this.renderBackButton()}
				{this.renderSignout()}
			</ul>
		)
	},
	renderBrand () {
		// TODO: support navbarLogo from keystone config

		const { brand, currentSectionKey } = this.props
		const className =
			currentSectionKey === 'dashboard'
				? 'primary-navbar__brand primary-navbar__item--active'
				: 'primary-navbar__brand'

		return (
			<PrimaryNavItem
				className={className}
				label='octicon-home'
				title={'Dashboard - ' + brand}
				to={Keystone.adminPath}
			>
				<span className='octicon octicon-home' />
			</PrimaryNavItem>
		)
	},
	// Render the navigation
	renderNavigation () {
		if (!this.props.sections || !this.props.sections.length) return null

		return this.props.sections.map(section => {
			// Get the link and the class name
			const href = section.lists[0].external
				? section.lists[0].path
				: `${Keystone.adminPath}/${section.lists[0].path}`
			const isActive =
				this.props.currentSectionKey &&
				this.props.currentSectionKey === section.key
			const className = isActive ? 'primary-navbar__item--active' : null
			const props = {}

			if (section.lists[0].external) {
				props.href = href
			} else {
				props.to = href
			}

			return (
				<PrimaryNavItem
					active={isActive}
					key={section.key}
					label={section.label}
					className={className}
					{...props}
				>
					{section.label}
				</PrimaryNavItem>
			)
		})
	},
	render () {
		if (!this.state.navIsVisible) return null

		return (
			<nav className='primary-navbar'>
				<Container
					clearFloatingChildren
					style={{ maxWidth: 'initial' }}
				>
					<ul
						className='app-nav app-nav--primary app-nav--left'
						style={{ width: '75%' }}
					>
						{this.renderBrand()}
						{this.renderNavigation()}
					</ul>
					{this.renderFrontLink()}
				</Container>
			</nav>
		)
	}
})

module.exports = PrimaryNavigation
